﻿using UnityEngine;
using System.Collections;

public class spoon : MonoBehaviour {


	public float speed = 20;
	public GameObject Player;
	private Vector3 andar;
	public float distancia = 2f;


	// Use this for initialization
	void Start () {

		Player = GameObject.Find("Player");
		andar.x = 1;
			
	}
	
	// Update is called once per frame
	void Update () {

		if (Mathf.Abs (transform.position.x - Player.transform.position.x) > distancia) {
			
			if (transform.position.x > Player.transform.position.x) {

				andar.x = -1;

			} else {

				andar.x = 1;
			}
		}

		transform.position += andar * speed * Time.deltaTime;
	
		
	}
}
