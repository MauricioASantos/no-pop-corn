﻿#pragma strict

 var maxX = 5;
 var minX = 0;

 
 private var tChange: float = 0; // force new direction in the first Update
 private var randomX: float;
 private var randomY: float;
 private var moveSpeed: float = 0.5;
 
 function Update () {
     // change to random direction at random intervals
     if (Time.time >= tChange){
         randomX = Random.Range(-5.0,6.0); // with float parameters, a random float
        
         tChange = Time.time + Random.Range(0.5,1.5);
     }
     transform.Translate(Vector3(randomX,randomY,0) * moveSpeed * Time.deltaTime);
     // if object reached any border, revert the appropriate direction
     if (transform.position.x >= maxX || transform.position.x <= minX) {
        randomX = -randomX;
     }

     // make sure the position is inside the borders
     transform.position.x = Mathf.Clamp(transform.position.x, minX, maxX);
   
 }