﻿using UnityEngine;
using System.Collections;

public class Movimentar : MonoBehaviour {

	public float speed = 3;
	public float jumpHeight = 5,distToGround,distToOut;
	private Rigidbody2D rg; 
	private static Animator ani;
	private SpriteRenderer sprenderer;
	private RaycastHit2D hit_right;
	public int temp = 0;
	public UnityEngine.UI.Text text;
	private bool gameOver = false,isGrounded=true;
	private AudioClip audioClip;
	private AudioClip audioClip1;
	private AudioClip audioClip2;
	private AudioClip audioClip3;
	private AudioClip audioClip4;
	private AudioClip audioClip5;
	private AudioSource audioSource;

	// Use this for initialization
	void Start () {
		sprenderer = GetComponent < SpriteRenderer> ();
		rg = GetComponent<Rigidbody2D>();
		ani = GetComponent<Animator> ();
		int jumpHash = Animator.StringToHash("Run");
		distToGround = GetComponent<BoxCollider2D>().bounds.extents.y;
		distToOut = GetComponent<BoxCollider2D>().bounds.extents.x;
		hit_right= Physics2D.Raycast(transform.position, -Vector2.right, distToOut + 0.1f);
		audioSource = GetComponent<AudioSource>();
		audioClip = (AudioClip)Resources.Load  ("Sounds/ALARME");
		audioClip1 = (AudioClip)Resources.Load ("Sounds/PANELA");
		audioClip2 = (AudioClip)Resources.Load ("Sounds/PEGAR PIMENTA");
		audioClip3 = (AudioClip)Resources.Load ("Sounds/CAIR NA PANELA");
		audioClip4 = (AudioClip)Resources.Load ("Sounds/VIRAR PIPOCA");
		audioClip5 = (AudioClip)Resources.Load ("Sounds/VIRANDO PIPOCA");
	
	}
	
	// Update is called once per frame
	void Update () {
		if (!gameOver) { 
			text.text = temp.ToString () + "ºC";
			if (Input.GetKey ("up")) {
				transform.rotation = Quaternion.identity; 
			}

			if (Input.GetAxisRaw ("Horizontal") != 0) {
				var andar = new Vector3 (Input.GetAxisRaw ("Horizontal"), 0, 0);
				//DeltaTime
				transform.position += andar * speed * Time.deltaTime;
				if (Input.GetAxisRaw ("Horizontal") > 0)
					sprenderer.flipX = false;
				else
					sprenderer.flipX = true;
				ani.SetBool ("isRunning", true);
				ani.SetBool ("isIdle", false);

			} else {
				ani.SetBool ("isIdle", true);
				ani.SetBool ("isRunning", false);
			}

			if (IsGrounded ()) {
				if (!ani.GetBool ("isIdle")) {
					
					ani.SetBool ("isIdle", true);
					audioSource.clip = audioClip3;
					audioSource.Play ();
				}
				ani.SetBool ("isRunning", false);

			}

			if (Input.GetKeyUp (KeyCode.Space)) {
				ani.Play ("Jump");
				rg.AddForce (Vector2.up * jumpHeight, ForceMode2D.Impulse);
				if(ani.GetBool("isGrounded")){
					ani.SetTrigger ("isJumping");
				}
			}
		}
	}
	public bool IsGrounded(){
		
		Vector3 tmploc = new Vector3 (transform.position.x, transform.position.y - distToGround-.05f);
		RaycastHit2D hit= Physics2D.Raycast(tmploc, Vector3.down/5, 0f);
		Debug.DrawRay(tmploc, Vector3.down/5,Color.green);
		if (hit.collider != null){
			if (hit.collider.tag == "Piso" || hit.collider.tag == "Corn" ) {
				Debug.Log (hit.collider.tag);
				ani.SetBool ("isGrounded", true);
				isGrounded = true;
				return true;
			} else {
				ani.SetBool ("isGrounded", false);
				isGrounded = false;
			}
		}
		return false;
	}
	void OnCollisionEnter2D(Collision2D hit){
		

		if (temp >= 100) {
			audioSource.PlayOneShot (audioClip5);
			ani.SetTrigger ("isPopping");
			ani.SetBool ("isDead", true);
			audioSource.clip = audioClip4;
			audioSource.Play ();
			gameOver = true;
			Invoke ("Stop", 2);

		}

		if (hit.gameObject.tag == "Piso") {
			//Bateu no chão
			Debug.Log("Chão!!!");
			temp += 25;
		}

	}

	void Stop(){
		
		Application.LoadLevel ("No Pop Corn");	
	}

	void OnCollisionExit(Collision hit){
		if (hit.gameObject.tag=="Piso"){
			;
		}
	}

	public bool IsDeitado(){
		RaycastHit2D hit_right= Physics2D.Raycast(transform.position, Vector2.right, distToOut + 0.1f);
		RaycastHit2D hit_left= Physics2D.Raycast(transform.position, Vector2.left, distToOut + 0.1f);
		bool chao = IsGrounded ();
		if ((hit_right.collider != null || hit_left.collider != null)){
			return true;
			Debug.Log ("Deitado!!!");
		}
		else
			return false;
	}
	void FixedUpdate(){
		


	}




	
}
