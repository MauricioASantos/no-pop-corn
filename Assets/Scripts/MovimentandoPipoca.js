﻿#pragma strict

var maxX = 5;
 var minX = 0;
 var maxY = 3.85;
 var minY =  0;
 var enable= true;
 private var tChange: float = 0; // force new direction in the first Update
 private var randomX: float;
 private var randomY: float;
 private var moveSpeed: float = 1.5;
 
 function Update () {
     // change to random direction at random intervals
     if(enable){
     if (Time.time >= tChange){
         randomX = Random.Range(-2.0,2.0); // with float parameters, a random float
         randomY = Random.Range(-1.0,2.0); //  between -2.0 and 2.0 is returned
         // set a random interval between 0.5 and 1.5
         tChange = Time.time + Random.Range(0.5,1.5);
     }
     transform.Translate(Vector3(randomX,randomY,0) * moveSpeed * Time.deltaTime);
     // if object reached any border, revert the appropriate direction
     if (transform.position.x >= maxX || transform.position.x <= minX) {
        randomX = -randomX;
     }
     if (transform.position.y >= maxY || transform.position.y <= minY) {
        randomY = -randomY;
     }
     // make sure the position is inside the borders
     transform.position.x = Mathf.Clamp(transform.position.x, minX, maxX);
     transform.position.y = Mathf.Clamp(transform.position.y, minY, maxY);
    }
 }